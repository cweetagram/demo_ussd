# A DEMO USSD APPLICATION

This is a demo ussd application using cweetagram ussd engine and [cweetagram chat session manger](https://bitbucket.org/cweetagram/chat-session-manager/src/master/)


## Installation

    $ git clone https://chancelito@bitbucket.org/cweetagram/demo_ussd.git
    $ cd demo_ussd
    $ composer install

## Hosting

You can copy the file on your server and point your application webhook to http://{your_url}/demo_ussd/ussd_json.php

## Credits
This is a property of [Cweetagram Solutions](https://cweetagramsolutions.com). For any queries contact:
[Chancel Shongo - CTO](mailto:chancel@cweetagram.co.za)
or
[Dev team](mailto:dev@cweetagram.co.za)
