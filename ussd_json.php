<?php

require_once __DIR__.'/vendor/autoload.php';

use CweetgramSolutions\Helper\ChatSession;

header('Content-Type: application/json');

$json = file_get_contents('php://input');
$data = json_decode($json);

ChatSession::findLastStep($data->sessionid);

if ((ChatSession::$process == 1) and (ChatSession::$step == 1)) {
    ChatSession::nextStep($data->sessionid, 1, 2);

    echo json_encode([
        'getInput' => true,
        'menuText' => 'Thank you for choosing cweetagram. Please choose your gender \n1.Male\n2.Female',
        'msisdn' => $data->msisdn,
        'code'	=> '200'
    ]);
    die();
}

if ((ChatSession::$process == 1) and (ChatSession::$step == 2)) {
    ChatSession::nextStep($data->sessionid, 1, 3);
    echo json_encode([
        'getInput' => true,
        'menuText' => 'Please choose your favourite fruit.\n1.Orange\n2.Mangoes\n3.Banana',
        'msisdn' => $data->msisdn,
        'code'	=> '200'
    ]);
    die();
}

if ((ChatSession::$process == 1) and (ChatSession::$step == 3)) {
    ChatSession::nextStep($data->sessionid, 1, 4);
    echo json_encode([
        'getInput' => true,
        'menuText' => 'Please choose your location.\n1.Johannesburg\n2.Durban\n3.Cape Town',
        'msisdn' => $data->msisdn,
        'code'	=> '200'
    ]);
    die();
}

if ((ChatSession::$process == 1) and (ChatSession::$step == 4)) {
    ChatSession::clearSteps($data->sessionid);
    echo json_encode([
        'getInput' => false,
        'menuText' => 'Thank you for taking this survey!',
        'msisdn' => $data->msisdn,
        'code'	=> '200'
    ]);
    die();
}
